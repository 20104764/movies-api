import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Person from "../../../../api/people/peopleModel";
import api from "../../../../index";
import people from "../../../../seedData/people";

const expect = chai.expect;
let db;
let page;
let personId;
let personName;

describe("People endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    beforeEach(async () => {
        try {
            await Person.deleteMany();
            await Person.collection.insertMany(people);
        } catch (err) {
            console.error(`failed to Load user Data: ${err}`);
        }
    });

    afterEach(() => {
        api.close();
    });


    describe("GET/api/people",()=>{
        it("Return 20 people",(done)=>{
            request(api)
                .get(`/api/people`)
                .set("Accept","application/json")
                .expect("Content-Type",/json/)
                .expect(200)
                .end((err,res)=>{
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(20);
                    done();
                });
        });
    });

    describe("GET/api/people/:id",()=>{
        describe("Id is valid",()=>{
            it("Return matched people",(done)=>{
                request(api)
                    .get(`/api/people/${people[0].id}`)
                    .set("Accept","application/json")
                    .expect("Content-Type",/json/)
                    .expect(200)
                    .end((err,res)=>{
                        expect(res.body).to.have.property("name",people[0].name);
                        done();
                    });
            });
            describe("Id is invalid",()=>{
                it("Return NOT-FOUND info",()=>{
                    return request(api)
                        .get(`/api/people/1`)
                        .expect("Content-Type",/json/)
                        .expect(404)
                        .expect({message:'The resource you requested could not be found.',status_code:404})
                });
            });
        });
    });

    describe("GET/api/people/tmdb/popular/page:page",()=>{
        describe("Page is valid",()=>{
            before(()=>{
                page=1;
            });
            it("Return matched list",()=>{
                return request(api)
                    .get(`/api/people/tmdb/popular/page${page}`)
                    .set("Accept","application/json")
                    .expect("Content-Type",/json/)
                    .expect(200)
                    .then((res)=>{
                        expect(res.body).to.have.property("page",page);
                        expect(res.body.results).to.be.a("array");
                        expect(res.body.results.length).to.equal(20);
                    });
            });
        });
    });

    describe("GET/api/people/tmdb/person/:id",()=>{
        describe("Id is valid",()=>{
            before(()=>{
                personId=19587, personName="Rumi Hiiragi";
            })
            it("Return matched details of person",()=>{
                return request(api)
                    .get(`/api/people/tmdb/person/${personId}`)
                    .set("Accept","application/json")
                    .expect("Content-Type",/json/)
                    .expect(200)
                    .then((res)=>{
                        expect(res.body).to.have.property("id",personId);
                        expect(res.body).to.have.property("name",personName);
                    });
            });
        });
        describe("Id is invalid",()=>{
            it("Return NOT-FOUND info",()=>{
                return request(api)
                    .get("/api/people/tmdb/person/a")
                    .set("Accept","application/json")
                    .expect(404)
                    .expect({
                        status_code:404,
                        message:"The resource you requested could not be found.",
                    });
            });
            it("Return Internal ServerError",()=>{
                return request(api)
                    .get("/api/people/tmdb/person/1145148")
                    .set("Accept","application/json")
                    .expect(500)
            });
        });
    });




    describe("GET/api/people/tmdb/person/:id/images",()=>{
        describe("Id is valid",()=>{
            before(()=>{
                personId=19587
            })
            it("Return person image",()=>{
                return request(api)
                    .get(`/api/people/tmdb/person/${personId}/images`)
                    .set("Accept","application/json")
                    .expect("Content-Type",/json/)
                    .expect(200)
                    .then((res)=>{
                        expect(res.body).to.have.property("id",personId);
                        expect(res.body).to.have.property("profiles");
                    });
            });
        });
        describe("Id is invalid",()=>{
            it("Return NOT-FOUND info",()=>{
                return request(api)
                    .get("/api/people/tmdb/person/a/images")
                    .set("Accept","application/json")
                    .expect(404)
                    .expect({
                        status_code:404,
                        message:"The resource you requested could not be found.",
                    });
            });
            it("Return Internal Server Error",()=>{
                return request(api)
                    .get("/api/people/tmdb/person/1145148/images")
                    .set("Accept","application/json")
                    .expect(500)
            });
        });
    });




    describe("GET /api/people/tmdb/person/:id/combined_credits", () => {
        describe("Id is invalid", () => {
            it("Return the NOT found message", () => {
                return request(api)
                    .get("/api/people/tmdb/person/a/combined_credits")
                    .set("Accept", "application/json")
                    .expect(404)
                    .expect({
                        status_code: 404,
                        message: "The resource you requested could not be found.",
                    });
            });
            it("Return Internal Server Error", () => {
                return request(api)
                    .get("/api/people/tmdb/person/1145148/combined_credits")
                    .set("Accept", "application/json")
                    .expect("Content-Type", "text/html; charset=utf-8")
                    .expect(500)
            });
        });
    });
});