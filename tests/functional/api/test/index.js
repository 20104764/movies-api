import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";
import User from "../../../../api/users/userModel";

const expect = chai.expect;
let db;

describe(" This EMPTY test Must BE ERROR OTHERWISE others will be error ", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });


  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test2",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });

  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe(" This EMPTY test Must BE ERROR OTHERWISE others will be error ", () => {
    it(" This EMPTY test Must BE ERROR OTHERWISE others will be error ", (done) => {
      request(api)
          .get("/api/users")
          .set("Accept", "application/json")
          .end((err, res) => {
            done();
          });
    });
  });


});
